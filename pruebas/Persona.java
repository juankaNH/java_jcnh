public class Persona {

    String nombre;
    int edad;

    void saluda() {

        System.out.println("Hola me llamo " + this.nombre + " y tengo " + this.edad + " años.");
    }

    void saluda(Persona p) {
        System.out.println("Hola " + p.nombre);
        System.out.println("Hola me llamo " + this.nombre + " y tengo " + this.edad + " años.");
        if (this.edad < p.edad) {
            int edad_diferente = p.edad - this.edad;
            System.out.println("Soy mas pequeño que tu " + edad_diferente + " años");
        } else {
            int edad_diferente =this.edad -  p.edad  ;
            System.out.println("Soy más grande que tu " + edad_diferente + " años");
        }
    }

}