public class Animalitos {
    public static void main(String[] args) {
        Animal toby = new Perro();
        Perro p = (Perro) toby;
        p.ladra();
        toby.duerme();

        Perromutante robot = new Perromutante();

        System.out.println("Patas de perro: " + p.getNumeroPatas());
        System.out.println("Patas de robot: " + robot.getNumeroPatas());
    }

    public void funt(){

    }
}