
/**
 * Esta clase contien los Hola Mundo graficos y por consola.
 * 
 * @author JuankaNH
 * @version v1.1 
 * @since JDK's
 * 
 */
import javax.swing.*;

public class HelloWord {

  private static String msje = "Hola Mundo";

  public static void main(String[] args) {
    System.out.println(msje);

    graficos();
  }

  static void graficos() {

    JFrame f = new JFrame(msje);
    f.setSize(400, 300);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.setVisible(true);
  }

}