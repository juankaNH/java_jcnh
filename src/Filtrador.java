import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.*;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Filtrador {
    static String a_filtrar;

    public static void main(String[] args) {

        try {
            if (args.length <= 0) {
                System.out.println("Escribe una palabra para filtrar.");
            } else {
                a_filtrar = args[0];
                System.out.println(a_filtrar);

            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        File fin = new File("motos.csv");
        String nombre_archivo = "contenidoExtraido/"+a_filtrar + ".csv";
        File salida_fichero = new File(nombre_archivo);

        Set<String> provincias = new TreeSet<String>();

        try (InputStreamReader fr = new InputStreamReader(new FileInputStream(fin), "UTF8");
                BufferedReader br = new BufferedReader(fr);) {

            String linea;

            do {
                linea = br.readLine();

                if (linea != null && linea.toLowerCase().contains(a_filtrar)) {

                    provincias.add(linea);
                }
            } while (linea != null);

        } catch (Exception e) {
            e.printStackTrace();
        }

       

        try (FileWriter fw = new FileWriter(salida_fichero); BufferedWriter bw = new BufferedWriter(fw);) {
           
            for (String provincia : provincias) {
                bw.write(provincia);
                bw.newLine();
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }
}
