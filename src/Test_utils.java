import java.util.Scanner;
import utils.*;

public class Test_utils {
    static String[] options = { "1. Dia de la Semana", "2. Numero del mes e idioma", "3. Mayor de una lista de enteros",
            "4. Mayor de una lista de chars", "5. Muestra una lista con separador",
            "6. Elige los numeros pares de una lista", "7. Crea un password Random del numero de caracteres elegidos",
            "8. Verifica que un pass tiene [A-Z][a-z][0-9]", "9. 4 metodos de encriptacion y desencriptacion",
            "10. Valida un email" };

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int num = 0;

        System.out.println("ELIGE TU FUNCION DE UTIL");
        for (String titulo : options) {
            System.out.println(titulo);
        }
        System.out.print("Introduce un numero: ");
        num = keyboard.nextInt();
        switch (num) {
        case 1:
            // diaSemana (int numero de dia, String idioma "ES", "CA", "EN")
            System.out.println(Utils.diaSemana(1, "ES"));
            System.out.println(Utils.diaSemana(2, "CA"));
            System.out.println(Utils.diaSemana(3, "EN"));
            break;
        case 2:
            System.out.println(Utils.diaSemana(4, "ES"));
            System.out.println(Utils.diaSemana(5, "CA"));
            System.out.println(Utils.diaSemana(6, "EN"));
            break;
        case 3:
            int[] enteros = { 2, 345, 6, 3, 23, 5, 65, 3, 45, 3, 4 };
            for (int numero : enteros) {
                System.out.print(numero + " ");
            }
            System.out.println("\nNumero mayor: " + Utils.mayor(enteros));
            break;
        case 4:
            char[] caracteres = { 'a', 'e', 'g', 'f', 't', 'y', 'f', 'e', 'd', 'v', 'h' };
            for (char carac : caracteres) {
                System.out.print(carac + " ");
            }
            System.out.println("\nCaracter mayor: " + Utils.mayor(caracteres));
            break;
        case 5:
            int[] nums = { 1, 4, 23, 16, 12 };
            System.out.println("Separador: >>>\nnumeros: ");
            for (int numeros : nums) {
                System.out.println(numeros);
            }
            System.out.println("Numeros con serparador: " + Utils.muestraArray(nums, ">>>"));
            break;
        case 6:
            int[] nums_pares = { 1, 4, 23, 16, 12, 59, 8, 62 };
            for (int n : nums_pares) {
                System.out.print(n + "\t");
            }
            int[] pares = Utils.pares(nums_pares);
            for (int numero : pares) {
                System.out.println("Numeros pares: " + numero);
            }
            break;
        case 7:
            System.out.print("Introduce un numero de tamaño de password: ");
            int numero_elegido = keyboard.nextInt();
            System.out.println(Utils.password(numero_elegido));
            break;
        case 8:
            System.out.print("Introduce un password: ");
            String pass_elegido = keyboard.nextLine();
            System.out.println(Utils.validaPassword(pass_elegido));
            break;
        case 9:
            System.out.print("Introduce una palabra: ");
            String palabra_elegida = keyboard.nextLine();
            System.out.println("Palabra elegida: ");
            String encriptado1 = Utils.encripta1(palabra_elegida);
            System.out.println("Encriptado 1: " + encriptado1);
            System.out.println("Desencriptado 1: " + Utils.desencripta1(encriptado1));
            String encriptado2 = Utils.encripta2(palabra_elegida);
            System.out.println("Encriptado 2: " + encriptado2);
            System.out.println("Desencriptado 2: " + Utils.desencripta1(encriptado2));
            // 
            break;
        case 10:
            System.out.print("Introduce un email: ");
            String email_elegido = keyboard.nextLine();
            System.out.println("Validez: " + Utils.validaPassword(email_elegido));
            break;
        default:
            System.out.println("ERROR de orden introducida.");
        }
    }
}