/**
 * Clase que contiene funciones estaticas como herramientas
 * 
 *  diaSemana (int numero de dia, String idioma "ES", "CA", "EN")
 *  mes(int numero de mes, String idioma "ES", "CA", "EN")
 *  mayor(int[] lista)
 *  mayor(char[] lista)
 *  muestraArray(int[] lista, String separador)
 *  pares(int[] lista)
 *  R_mayusculas()
 *  R_minusculas()
 *  R_numeros()
 *  password(int tamaño de la contraseña)
 *  verificaEmail(string email)
 *  validaPassword(String password) //comprueba que tiene [a-z][A-Z][0-9]
 *  encripta1(String texto)
 *  desencripta1(String texto encriptado)
 *  encripta2(String texto)
 *  desencripta2(String texto encriptado)
 *  encripta3(String texto, String cadena de referencia para encriptar)
 *  desencripta3(String texto encriptado, String cadena de referida para desencriptar)
 *  encripta4(String texto)
 *  desencripta4(String texto encriptado)
 * 
 * @author JuankaNH
 * 
 */

package utils;


public class Utils {

    public static String[] semana_es = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
    public static String[] semana_ca = { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Disabte",
            "Diumenge" };
    public static String[] semana_en = { "Monday", "Thuesday", "Wenesday", "Thrusday", "Friday", "Saturday", "Sunday" };

    public static String[] mes_es = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
            "Septiembre", "Octubre", "Noviembre", "Diciembre" };
    public static String[] mes_ca = { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost",
            "Septembre", "Octubre", "Novembre", "Decembre" };
    public static String[] mes_en = { "Genuari", "Febrary", "March.", "April", "May", "June", "July", "August",
            "September", "October", "November", "December" };

    public static String diaSemana(int dia, String idioma) {
        String dia_devuelto = "";
        if (dia > 0 & dia <= 6) {
            if (idioma == "es" | idioma == "ES") {
                dia_devuelto = semana_es[dia];
            } else if (idioma == "ca" | idioma == "CA") {
                dia_devuelto = semana_ca[dia];
            } else if (idioma == "en" | idioma == "EN") {
                dia_devuelto = semana_en[dia];
            } else {
                System.out.println("Idioma elegido incorrecto.");
            }
        } else {
            System.out.println("Dia de la semana elegido incorrecto.");
        }
        return dia_devuelto;
    }

    public static String mes(int mes, String idioma) {
        String mes_devuelto = "";
        if (mes > 0 & mes <= 11) {
            if (idioma == "es" | idioma == "ES") {
                mes_devuelto = mes_es[mes];
            } else if (idioma == "ca" | idioma == "CA") {
                mes_devuelto = mes_ca[mes];
            } else if (idioma == "en" | idioma == "EN") {
                mes_devuelto = mes_en[mes];
            } else {
                System.out.println("Idioma elegido incorrecto.");
            }
        } else {
            System.out.println("Dia del mes elegido incorrecto.");
        }
        return mes_devuelto;
    }

    public static int mayor(int[] lista) {
        int mayor = lista[0];
        for (int i = 0; i < lista.length; i++) {
            if (mayor < lista[i]) {
                mayor = lista[i];
            } else {
                continue;
            }
        }
        return mayor;
    }

    public static char mayor(char[] lista) {
        char caracter = 's';
        int[] array_carac = new int[lista.length];
        for (int i = 0; i < array_carac.length; i++) {
            array_carac[i] = (int) lista[i];
            // System.out.println( array_carac[i] + " | " + lista[i]);
        }
        caracter = (char) mayor(array_carac);
        return caracter;
    }

    public static String muestraArray(int[] numeros, String separador) {
        String cadena = "";

        for (int i = 0; i < numeros.length; i++) {
            if (i != numeros.length - 1) {
                cadena += numeros[i] + separador;
            } else {
                cadena += numeros[i];
            }
        }

        return cadena;

    }

    public static int[] pares(int[] lista) {

        int contador = 0;
        for (int numero : lista) {
            if (numero % 2 == 0) {
                contador++;
            }
        }
        // System.out.println("Contador: " + contador);
        int[] resultado = new int[contador];
        contador = 0;
        for (int numero : lista) {
            if (numero % 2 == 0) {
                resultado[contador] = numero;
                contador++;
            }
        }

        return resultado;
    }

    public static int R_mayusculas() {
        return (int) ((Math.random() * 25) + 65);
    }

    public static int R_minusculas() {
        return (int) ((Math.random() * 25) + 97);
    }

    public static int R_numeros() {
        return (int) ((Math.random() * 9) + 48);
    }

    public static String password(int largada) {
        // [A-Z] --> 65,90 [a-z] --> 97,122 [0-9] --> 48,57
        String pass = "";
        while (true) {
            for (int i = 0; i < largada; i++) {
                int r = (int) ((Math.random() * 3) + 1);
                if (r == 1) {
                    pass += (char) R_mayusculas();
                } else if (r == 2) {
                    pass += (char) R_minusculas();
                } else if (r == 3) {
                    pass += (char) R_numeros();
                }

            }
            if (pass.matches(".*[a-z]+.*") && pass.matches(".*[A-Z]+.*") && pass.matches(".*[0-9]+.*")) {
                break;
            }
            pass = "";
        }

        return pass;
    }

    public static boolean verificaEmail(String mail){
        boolean estado = false;
        if(mail.matches(".*...@(gmail|hotmail).(com|es)$")){
            estado = true;
        }
        return estado;
    }

    public static boolean validaPassword(String pass){
        boolean estado = false;
        if(pass.length() >= 7){
            if (pass.matches(".*[a-z]+.*") && pass.matches(".*[A-Z]+.*") && pass.matches(".*[0-9]+.*")) {
                estado = true;
            }else{
                System.out.println("Tiene que contener Mayusculas, Minusculas y Numeros");
            }
        }else{
            System.out.println("ERROR: minimo 8 caracteres");
        }

        return estado;
    }

    public static String encripta1(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            if (letra == 'A' | letra == 'a') {
                palabra += "1";
            } else if (letra == 'E' | letra == 'e') {
                palabra += "2";
            } else if (letra == 'I' | letra == 'i') {
                palabra += "3";
            } else if (letra == 'O' | letra == 'o') {
                palabra += "4";
            } else if (letra == 'U' | letra == 'u') {
                palabra += "5";
            } else {
                palabra += letra;
            }
        }
        return palabra;
    }

    public static String desencripta1(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            if (letra == '1') {
                palabra += "a";
            } else if (letra == '2') {
                palabra += "e";
            } else if (letra == '3') {
                palabra += "i";
            } else if (letra == '4') {
                palabra += "o";
            } else if (letra == '5') {
                palabra += "u";
            } else {
                palabra += letra;
            }
        }
        return palabra;
    }

    public static String encripta2(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            palabra += (char) (letra + 1);
        }
        return palabra;
    }

    public static String desencripta2(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            palabra += (char) (letra - 1);
        }
        return palabra;
    }

    public static String encripta3(String s, String cadena) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
             System.out.println("L: "+ (letra)+" | " + ((int)letra)+" | " + (letra +((cadena.length() *2 ) -3)));

            palabra += (char) ((letra + (cadena.length() * 2) - 3));
        }
        return palabra;
    }

    public static String desencripta3(String s, String cadena) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            // System.out.println("L: " + (letra)+" | " + ((int)letra)+" | " + ((letra -((cadena.length() /2 ) +3)-8))+ " | " + ((letra + (cadena.length() +3)/2)));

            palabra += (char) (letra - (cadena.length() * 2) +3);
            // ((cadena.length() / 2) + 3) - (8)));
        }
        return (String) palabra;
    }

    public static String encripta4(String s) {
        String palabra = "";
        String encriptador = "😘";
        char[] enc = encriptador.toCharArray();
        int i = 0;
        for (char letra : s.toCharArray()) {
            palabra += (char) ((letra + enc[i]));
            i++;
            //System.out.println("i: " + i + " | " + s.length() + " | " + enc.length);
            if (i >= enc.length - 1) {
                i = 0;
            }
        }
        return palabra;
    }

    public static String desencripta4(String s) {
        String palabra = "";
        String encriptador = "😘";
        int i = 0;
        char[] enc = encriptador.toCharArray();
        for (char letra : s.toCharArray()) {
            palabra += (char) ((letra - enc[i]));
            i++;
            if (i >= enc.length - 1) {
                i = 0;
            }
        }
        return palabra;
    }

}