import java.util.*;


public class Personas{
    public static void main(String[] args) {
        Persona p1 = new Persona("marta", 22);
        Persona p2 = new Persona("albert", 25);
        Persona p3 = new Persona("marta", 12);
        Persona p4 = new Persona("albert", 25);
        Persona p5 = new Persona("marta", 22);
       

        Set<Persona> lista = new HashSet<Persona>();

        lista.add(p1);
        lista.add(p2);
        lista.add(p3);
        lista.add(p4);
        lista.add(p5);

        System.out.println("total personas: " + lista.size());
    }
}