package com.codesplai;

import java.util.Random;
import java.util.Scanner;

public class Juegos {

    public static Jugador jugador1;
    public static Jugador jugador2;
    public static int partidas;
    public static Scanner keyboard = new Scanner(System.in);

    /**
     * pide nombre de jugador, 1 o 2 crea objeto "Jugador" y lo asigna a jugador1 o
     * jugador2
     */
    public static void pideJugador(int numJugador) {
        // pedimos nombre de jugador
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador(nombre);

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        } else {
            Juegos.jugador2 = j;
        }
    }

    /**
     * muestra menu de juegos y pide opción
     */
    public static void menu() {
        Limpiza_de_terminal();
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedra, papel, tijeras");
        System.out.println("3: Ruleta VACILONA!!");
        System.out.println("*******************");
        // pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
        } while (opcion < 1 || opcion > 3);

        switch (opcion) {
        case 1:
            Limpiza_de_terminal();
            Juegos.caraCruz();
            break;
        case 2:
            Limpiza_de_terminal();
            Juegos.P_P_T();
            break;
        case 3:
            Limpiza_de_terminal();
            Juegos.ruleta();
            break;
        default:
            break;
        }
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        Random rnd = new Random();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Cara (C) o cruz(X) ?  ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            // sea cual sea la probabilidad es un 50%...
            // obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas);
        System.out.println(resumen);
    }

    /**
     * juego piedra papel tijeras
     */
    public static void P_P_T() {
        Juegos.pideJugador(1); // solo interviene un jugador en este juego
        Juegos.partidas = 5; // partidas por defecto en este juego

        System.out.println();
        System.out.println("Piedra papel tijeras:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();
        // bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Piedra (P) papel(A) tijera(T) ?  ");
            // pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
            int rnd = (int) (Math.random() * 3) + 1;

            if (rnd == 1) {
                if (apuesta.equals("P")) {
                    Juegos.jugador1.empates++;
                    System.out.println("Maquina: Piedra Jugador: Piedra Resultado: " + "EMPATASTES.");
                } else if (apuesta.equals("A")) {
                    Juegos.jugador1.ganadas++;
                    System.out.println("Maquina: Piedra Jugador: Papel Resultado: " + "GANASTE.");
                } else if (apuesta.equals("T")) {
                    Juegos.jugador1.perdidas++;
                    System.out.println("Maquina: Piedra Jugador: Tijeras Resultado: " + "PERDISTE.");
                } else {
                    System.out.println("Error de dato introducio:");
                }
            } else if (rnd == 2) {
                if (apuesta.equals("P")) {
                    Juegos.jugador1.perdidas++;
                    System.out.println("Maquina: Papel Jugador: Piedra Resultado: " + "PERDISTE.");
                } else if (apuesta.equals("A")) {
                    Juegos.jugador1.empates++;
                    System.out.println("Maquina: Papel Jugador: Papel Resultado: " + "EMPATASTES.");
                } else if (apuesta.equals("T")) {
                    Juegos.jugador1.ganadas++;
                    System.out.println("Maquina: Papel Jugador: Tijera Resultado: " + "GANASTE.");
                } else {
                    System.out.println("Error de dato introducio:");
                }
            } else if (rnd == 3) {
                if (apuesta.equals("P")) {
                    Juegos.jugador1.ganadas++;
                    System.out.println("Maquina: Tijeras Jugador: Piedra Resultado: " + "GANASTE.");
                } else if (apuesta.equals("A")) {
                    Juegos.jugador1.perdidas++;
                    System.out.println("Maquina: Tijeras Jugador: Papel Resultado: " + "PERDISTE.");
                } else if (apuesta.equals("T")) {
                    Juegos.jugador1.empates++;
                    System.out.println("Maquina: Tijeras Jugador: Tijeras Resultado: " + "EMPATASTES.");
                } else {
                    System.out.println("Error de dato introducio:");
                }
            } else {
                System.out.println("Random: " + rnd + "ERROR: jugada ilogica");
            }
            Juegos.jugador1.partidas++;
        }

        // creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas, %d perdidas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas, Juegos.jugador1.perdidas);
        System.out.println(resumen);

    }

    /**
     * juego ruleta vacilona
     */
    public static void ruleta() {
        System.out.println("This is la rouleta de la fortuna !!\nLO VAS A PERDER TODO AJAJAJAJAJ\n");
        System.out.println("Cuanto dinero quieres perder? ");
        int guita = keyboard.nextInt();
        System.out.println("\nPerdon como te llamas? Para saber a quien voy a desplumar.");

        Juegos.pideJugador(1);
        Juegos.jugador1.saldo = guita;
        Limpiza_de_terminal();
        String intro = String.format("Te llamas %s y te voy a ventilar %d euros. MUY BIEN CHICO",
                Juegos.jugador1.nombre, Juegos.jugador1.saldo);
        System.out.println(intro);
        boolean acabar_juego = true;
        do {

            acabar_juego = apostar();

            Resultados[] Rsusts = Ruleta.tirada(Juegos.jugador1.get_apuestas());

            Limpiza_de_terminal();
            System.out.println("Acabar juego: " + acabar_juego);
            for (Resultados resul : Rsusts) {
                if (resul != null) {
                    // System.out.println("Resultado: " + resul.ap.name + " " + resul.ap.money + " :
                    // " + resul.stado);
                    resul.rest_info();

                    if (resul.stado == "Ganaste.") {
                        int cantidad_dinero = resul.ap.money * 2;
                        System.out.println("Resultado_g: " + resul.ap.money + " --> " + resul.ap.money * 2 + " | "
                                + Juegos.jugador1.saldo);
                        System.out.println(
                                "----------------------------------------------------------------------------");
                        Juegos.jugador1.saldo += cantidad_dinero;
                        resul.set_g_p(cantidad_dinero);
                        Juegos.jugador1.ganadas++;
                    } else {
                        System.out.println("Resultado_p: " + resul.ap.money + " --> Saldo: " + Juegos.jugador1.saldo
                                + " --> Saldo_Resultante: " + (Juegos.jugador1.saldo - resul.ap.money));
                        System.out.println(
                                "----------------------------------------------------------------------------");
                        Juegos.jugador1.saldo = Juegos.jugador1.saldo - resul.ap.money;
                        resul.set_g_p(-1 * (resul.ap.money));
                        Juegos.jugador1.perdidas++;
                    }
                }
            }
            Juegos.jugador1.set_resultados(Rsusts);
            Juegos.jugador1.partidas++;
            System.out.println("Saldo: " + Juegos.jugador1.saldo);
            System.out.println("Apostado: " + Juegos.jugador1.apostado);
            Juegos.jugador1.clean_apuestas();
            Juegos.jugador1.apostado = 0;

        } while (Juegos.jugador1.saldo > 0 && acabar_juego);
        resultado_final();
    }
    
    private static void resultado_final() { //Debug de los resultados finales

        String resumen = String.format("Jugador %s, %d partidas, %d ganadas, %d perdidas.", Juegos.jugador1.nombre,
                Juegos.jugador1.partidas, Juegos.jugador1.ganadas, Juegos.jugador1.perdidas);
        System.out.println(resumen);
        System.out.println("----------------------------------------------------------------------------");
        Juegos.jugador1.print_resultados();
    }

    private static String Renombrar_valor(String v) {
        String valor = "";
        switch (v) {
        case "A":
            valor = "Rojo/Negro";
            break;
        case "B":
            valor = "Par/Impar";
            break;
        case "C":
            valor = "Falta/Pasa";
            break;
        case "X":
            valor = "Dejaste de apostar";
            break;
        default:
            valor = "Elige bien capullo!!";
            break;
        }
        return valor;
    }

    public static boolean apostar() {   //Crea las apuestas de los jugadores
        // Rojo/Negro Par/Impar Falta/Pasa
        boolean fin = true;
        boolean f_final = true;
        do {

            System.out.println(
                    "Cual sera tu apuesta? (A)Rojo/Negro (B)Par/Impar (C)Falta/Pasa (X)Finalizar apuesta (Q)Finalizar partida.");

            String option = keyboard.next();
            option = option.toUpperCase();
            System.out.println("Eleccion tuya: " + Renombrar_valor(option));
            if (option.equals("A")) {
                System.out.println("Cual sera tu apuesta? (1)Rojo (2)Negro");
                int eleccion_color = keyboard.nextInt();
                switch (eleccion_color) {
                case 1:
                    System.out.println("Has elegido Rojo");
                    System.out.println("Cuanta guita?");
                    int dinero_r = keyboard.nextInt();
                    if (Juegos.jugador1.apostado < Juegos.jugador1.saldo) {
                        Juegos.jugador1.apostado += dinero_r;
                        // Juegos.jugador1.saldo -= dinero_r;
                        apuesta ape = new apuesta("Rojo", dinero_r);
                        Juegos.jugador1.set_apuesta(ape);
                    } else {
                        System.out.println("Pringado no tienes pasta suficiente!!");
                    }
                    break;
                case 2:
                    System.out.println("Has elegido Negro");
                    System.out.println("Cuanta guita?");
                    int dinero_n = keyboard.nextInt();
                    if (Juegos.jugador1.apostado < Juegos.jugador1.saldo) {
                        Juegos.jugador1.apostado += dinero_n;
                        // Juegos.jugador1.saldo -= dinero_n;
                        apuesta ape = new apuesta("Negro", dinero_n);
                        Juegos.jugador1.set_apuesta(ape);
                    } else {
                        System.out.println("Pringado no tienes pasta suficiente!!");
                    }
                    break;
                default:
                    System.out.println("Cagado eso no es rojo ni negro!!");
                    break;
                }
            } else if (option.equals("B")) {
                System.out.println("Cual sera tu apuesta? (1)Par (2)Impar");
                int eleccion_pares = keyboard.nextInt();
                switch (eleccion_pares) {
                case 1:
                    System.out.println("Has elegido Par");
                    System.out.println("Cuanta guita?");
                    int dinero_p = keyboard.nextInt();
                    if (Juegos.jugador1.apostado < Juegos.jugador1.saldo) {
                        Juegos.jugador1.apostado += dinero_p;
                        // Juegos.jugador1.saldo -= dinero_p;
                        apuesta ape = new apuesta("Par", dinero_p);
                        Juegos.jugador1.set_apuesta(ape);
                    } else {
                        System.out.println("Pringado no tienes pasta suficiente!!");
                    }
                    break;
                case 2:
                    System.out.println("Has elegido Impar");
                    System.out.println("Cuanta guita?");
                    int dinero_i = keyboard.nextInt();
                    if (Juegos.jugador1.apostado < Juegos.jugador1.saldo) {
                        Juegos.jugador1.apostado += dinero_i;
                        // Juegos.jugador1.saldo -= dinero_i;
                        apuesta ape = new apuesta("Impar", dinero_i);
                        Juegos.jugador1.set_apuesta(ape);
                    } else {
                        System.out.println("Pringado no tienes pasta suficiente!!");
                    }
                    break;
                default:
                    System.out.println("Cagado eso no es par ni impar!!");
                    break;
                }
            } else if (option.equals("C")) {
                System.out.println("Cual sera tu apuesta? (1)Falta (2)Pasa");
                int eleccion_faltas = keyboard.nextInt();
                // System.out.println("Y tu numero? [0-36]");
                // int eleccion_numero = keyboard.nextInt();
                int eleccion_numero = 0;
                switch (eleccion_faltas) {
                case 1:
                    System.out.println("Has elegido Falta");
                    System.out.println("Cuanta guita?");
                    int dinero_f = keyboard.nextInt();
                    if (Juegos.jugador1.apostado < Juegos.jugador1.saldo) {
                        Juegos.jugador1.apostado += dinero_f;
                        // Juegos.jugador1.saldo -= dinero_f;
                        apuesta ape = new apuesta("Falta", dinero_f, eleccion_numero);
                        Juegos.jugador1.set_apuesta(ape);
                    } else {
                        System.out.println("Pringado no tienes pasta suficiente!!");
                    }
                    break;
                case 2:
                    System.out.println("Has elegido Pasa");
                    System.out.println("Cuanta guita?");
                    int dinero_p = keyboard.nextInt();
                    if (Juegos.jugador1.apostado < Juegos.jugador1.saldo) {
                        Juegos.jugador1.apostado += dinero_p;
                        // Juegos.jugador1.saldo -= dinero_p;
                        apuesta ape = new apuesta("Pasa", dinero_p, eleccion_numero);
                        Juegos.jugador1.set_apuesta(ape);
                    } else {
                        System.out.println("Pringado no tienes pasta suficiente!!");
                    }
                    break;
                default:
                    System.out.println("Cagado eso no es ni falta ni pasa!!");
                    break;
                }
            } else if (option.equals("X")) {
                System.out.println("Dejaste de apostar marica.");
                fin = false;
            } else if (option.equals("Q")) {
                System.out.println("Dejaste de jugar moñas.");

                fin = false;
                f_final = false;
            } else {
                System.out.println("ERROR de eleccion de apuesta\t APUESTA TU HUEVOS SI TE ATREVES JAJAJAJA");
            }
        } while (fin | Juegos.jugador1.saldo <= 0);
        Juegos.jugador1.printa_lista();
        System.out.println("Saldo: " + Juegos.jugador1.saldo);
        if (f_final) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * juego limpiador de pantalla
     */
    public static void Limpiza_de_terminal() { // Proceso para limpiar el terminal
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {

        }
    }
}
