package com.codesplai;

import javax.security.auth.AuthPermission;

public class Resultados {

    apuesta ap;
    String stado;
    int g_p = 0;
    int numero_makina = 99;
    String color_makina = "";

    public Resultados(apuesta apu) {
        this.ap = apu;
    }

    public Resultados(apuesta apu, int n, String c) {
        this.ap = apu;
        this.numero_makina = n;
        this.color_makina = c;
        System.out.println("Apuesta: " + this.ap.name + " " + this.ap.money + " VS " + this.numero_makina + " "
                + this.color_makina);

    }

    public void set_g_p(int dinero) {
        this.g_p = dinero;
    }

    public void rest_info() {
        System.out.println(this.ap.money + " al " + this.ap.name);
        System.out.println("...\nSale...\n" + this.numero_makina + ":" + this.color_makina);
        System.out.println(this.stado);
        System.out.println("----------------------------------------------------------------------------");
    }

    @Override
    public String toString() {
        return String.format("Apuesta: %s %d Ruleta: %d %s Estado: %s %d",this.ap.name,this.ap.money, this.numero_makina, this.color_makina ,this.stado, this.g_p);
    }
}