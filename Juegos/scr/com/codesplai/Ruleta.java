package com.codesplai;

public class Ruleta {

    public static int[] rojos = { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36 };
    public static int[] negros = { 2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35 };

    // METODOS PUBLICOS
    public static Resultados[] tirada(apuesta[] apuestas) {

        int rnd = (int) (Math.random() * 37);
        String info_tirada = String.format("Ha salido en la Ruleta numero %d color %s", rnd, color_numero(rnd));
        System.out.println(info_tirada);
        if (rnd == 0) {
            Resultados[] resultados = new Resultados[apuestas.length];
            int cont = 0;
            for (apuesta ap : apuestas) {
                Resultados r = new Resultados(ap);
                r.stado = "Perdiste.";
                resultados[cont] = r;
            }
            System.out.println("\n\nOHHH SIII NENAAAAAA!!! LA BANCA GANA \nTIRI TIRI TIRIIIII...!!\n\n");
             return resultados;

        } else {
             Resultados[] resultados = Comprobacion_de_la_juagada(apuestas, rnd,color_numero(rnd));
            // Comprobacion_de_la_juagada(apuestas, rnd, color_numero(rnd));
             return resultados;
        }

    }

    // METODOS PRIVADOS
    private static Resultados[] Comprobacion_de_la_juagada(apuesta[] apuestas, int numero_r, String color_r) {
        Resultados[] results = new Resultados[apuestas.length];
        int cont = 0;
        // System.out.println("Tamano: " + apuestas.length);
        for (int i = 0; i < apuestas.length; i++) {
            if (apuestas[i] != null) {

                // System.out.println("C: " + apuestas[i].name.equals("Rojo") + " | " + apuestas[i].name);
                if (apuestas[i].name.equals("Rojo") | apuestas[i].name.equals("Negro")) {
                    Resultados r = new Resultados(apuestas[i], numero_r, color_r);
                    // System.out.println(apuestas[i].name + " | " + color_r);
                    if (apuestas[i].name == color_r) {
                        System.out.println("Ganaste.");
                        r.stado = "Ganaste.";
                    } else {
                        // System.out.println("Perdiste.");
                        r.stado = "Perdiste.";
                    }
                    results[cont] = r;
                    cont++;
                } else if (apuestas[i].name.equals("Par") | apuestas[i].name.equals("Impar")) {
                    Resultados r = new Resultados(apuestas[i], numero_r, color_r);
                    if (apuestas[i].name.equals(pares(numero_r))) {
                        r.stado = "Ganaste.";
                    } else {
                        r.stado = "Perdiste.";
                    }
                    results[cont] = r;
                    cont++;
                } else if (apuestas[i].name.equals("Falta") | apuestas[i].name.equals("Pasa")) {
                    Resultados r = new Resultados(apuestas[i], numero_r, color_r);
                    if (apuestas[i].name.equals(Faltaspasas(numero_r))) {
                        r.stado = "Ganaste.";
                    } else {
                        r.stado = "Perdiste.";
                    }

                    results[cont] = r;
                    cont++;
                } else {
                    Resultados r = new Resultados(apuestas[i], numero_r, color_r);
                    r.stado = "Perdiste.";
                    results[cont] = r;
                    cont++;
                }
            }
        }
        return results;
    }
 
    private static String color_numero(int n) {
        String color = "";
        for (int numero : rojos) {
            if (n == numero) {
                color = "Rojo";
            } else {
                color = "Negro";
            }
        }
        return color;
    }

    private static String pares(int i) {
        String valor = "";
        if (i % 2 == 0) {
            valor = "Par";
        } else {
            valor = "Impar";
        }

        return valor;
    }

    private static String Faltaspasas(int i) {
        String valor = "";
        if (i >= 1 & i <= 18) {
            valor = "Falta";
        } else {
            valor = "Pasa";
        }

        return valor;
    }

}