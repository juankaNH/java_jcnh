package com.codesplai;

import java.util.ArrayList;

public class Jugador {

    String nombre;
    int ganadas;
    int empates;
    int perdidas;
    int partidas;
    int saldo;
    int apostado;

    private static int max_apuesta = 10;
    private static ArrayList<apuesta> apuestas_creadas = new ArrayList<apuesta>();
    private static apuesta[] ap = new apuesta[9];
    private static int apuesta_creadas = 0;

    private static ArrayList<Resultados> R_de_apuestas = new ArrayList<Resultados>();

    public Jugador(String nombre) {
        this.nombre = nombre;
        this.ganadas = 0;
        this.empates = 0;
        this.partidas = 0;
        this.perdidas = 0;
    }

    // APUESTAS CREADAS
    public static void set_apuesta(apuesta a) {
        if (apuesta_creadas <= max_apuesta) {

            ap[apuesta_creadas] = a;
            apuesta_creadas++;
            // apuestas_creadas.add(a);
        }
    }

    public static apuesta[] get_apuestas() {
        return ap;
    }

    public static void printa_lista() {
        System.out.println(" lista de apuestas....:");
        // System.out.println(apuestas_creadas);
        for (int i = 0; i < apuesta_creadas; i++) {
            System.out.println("Apuesta " + i + " : " + ap[i].name + " | " + ap[i].money + " | " + ap[i].numero);
        }

    }

    public static void clean_apuestas() {
        apuesta_creadas = 0;
        for (int i = 0; i < ap.length; i++) {
            ap[i] = null;
        }
    }

    // LISTA DE RESULTADOS FINALES
    public static void set_resultados(Resultados[] r) {
        for (Resultados re : r) {
            R_de_apuestas.add(re);
        }
    }

    public static void print_resultados() {
        // System.out.println(R_de_apuestas.toString());

        for (Resultados r : R_de_apuestas) {
             System.out.println(r != null ? r.toString() : "");
            // if(r != null){
            //     r.toString();
            // }
        }

    }

    public static ArrayList<Resultados> get_resultados() {
        return R_de_apuestas;
    }
}
