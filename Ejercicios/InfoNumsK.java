import java.util.Scanner;
public class InfoNumsK{
    
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num=1;
        String cadena = "";
        do {
            System.out.printf("Entra un num: ");
            try {  
                    num = keyboard.nextInt(); 
                    cadena += num + ";"; 
                } catch (Exception e) {  
                    System.out.println("***Dato incorrecto - 0 para salir***");
                    keyboard.next();
                } 
        } while (num != 0);

        String[] numeros_cadena = cadena.split(";");
        int[] numero = new int[numeros_cadena.length];
        for(int i = 0; i < numeros_cadena.length ;i++){
            numero[i]= Integer.parseInt( numeros_cadena[i]);
        }
        reproducir_numeros(numero);
        numero_mayor(numero);
        numero_menor(numero);
        numero_media(numero);
        System.out.println("La suma es "+ total);
        keyboard.close();
    }

    
    static void reproducir_numeros(int[] numeros) {
        System.out.println("Hay " + numeros.length + " numeros.");
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + " | ");

        }
    }

    static void numero_mayor(int[] numeros) {
        int mayor  = numeros[0];
        for (int i = 0; i < numeros.length; i++) {
            if(mayor < numeros[i]){
                mayor = numeros[i];
            }else{
                continue;
            }
        }
        System.out.println("\nEl mayor es " + mayor);
    }

    static void numero_menor(int[] numeros) {
        int menor = numeros[0];
        for (int i = 0; i < numeros.length; i++) {
            if(menor > numeros[i]){
                menor = numeros[i];
            }else{
                continue;
            }
        }
        System.out.println("\nEl menor es " + menor);
    }

    static void numero_media(int[] numeros) {
        float total = 0;
        for (int i = 0; i < numeros.length; i++) {
            total += numeros[i];

        }

        System.out.println("\nTotal de los numeros: " + total + " la media es:" + (total / numeros.length));

    }

}