import java.util.Scanner;
public class Test_encriptacion {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String s = new String( keyboard.nextLine());
        System.out.println(s);

        String encriptado_1 = Encripta.encripta1(s);
        String encriptado_2 = Encripta.encripta2(s);
        String encriptado_3 = Encripta.encripta3(s,s);
        String encriptado_4 = Encripta.encripta4(s);

        System.out.println("encriptado_1: " +encriptado_1);
        System.out.println("desencripta1: "+Encripta.desencripta1(encriptado_1));
        System.out.println("encriptado_2: " +encriptado_2);
        System.out.println("desencripta2: "+Encripta.desencripta2(encriptado_2));
        System.out.println("encriptado_3: " +encriptado_3);
        System.out.println("desencripta3: "+Encripta.desencripta3(encriptado_3,s));
        System.out.println("encriptado_4: " +encriptado_4);
        System.out.println("desencripta4: "+Encripta.desencripta4(encriptado_4));
       
    }
}