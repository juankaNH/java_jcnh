public class Encripta {

    public static String encripta1(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            if (letra == 'A' | letra == 'a') {
                palabra += "1";
            } else if (letra == 'E' | letra == 'e') {
                palabra += "2";
            } else if (letra == 'I' | letra == 'i') {
                palabra += "3";
            } else if (letra == 'O' | letra == 'o') {
                palabra += "4";
            } else if (letra == 'U' | letra == 'u') {
                palabra += "5";
            } else {
                palabra += letra;
            }
        }
        return palabra;
    }

    public static String desencripta1(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            if (letra == '1') {
                palabra += "a";
            } else if (letra == '2') {
                palabra += "e";
            } else if (letra == '3') {
                palabra += "i";
            } else if (letra == '4') {
                palabra += "o";
            } else if (letra == '5') {
                palabra += "u";
            } else {
                palabra += letra;
            }
        }
        return palabra;
    }

    public static String encripta2(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            palabra += (char) (letra + 1);
        }
        return palabra;
    }

    public static String desencripta2(String s) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            palabra += (char) (letra - 1);
        }
        return palabra;
    }

    public static String encripta3(String s, String cadena) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
             System.out.println("L: "+ (letra)+" | " + ((int)letra)+" | " + (letra +((cadena.length() *2 ) -3)));

            palabra += (char) ((letra + (cadena.length() * 2) - 3));
        }
        return palabra;
    }

    public static String desencripta3(String s, String cadena) {
        String palabra = "";
        for (char letra : s.toCharArray()) {
            // System.out.println("L: " + (letra)+" | " + ((int)letra)+" | " + ((letra -((cadena.length() /2 ) +3)-8))+ " | " + ((letra + (cadena.length() +3)/2)));

            palabra += (char) (letra - (cadena.length() * 2) +3);
            // ((cadena.length() / 2) + 3) - (8)));
        }
        return (String) palabra;
    }

    public static String encripta4(String s) {
        String palabra = "";
        String encriptador = "😘";
        char[] enc = encriptador.toCharArray();
        int i = 0;
        for (char letra : s.toCharArray()) {
            palabra += (char) ((letra + enc[i]));
            i++;
            //System.out.println("i: " + i + " | " + s.length() + " | " + enc.length);
            if (i >= enc.length - 1) {
                i = 0;
            }
        }
        return palabra;
    }

    public static String desencripta4(String s) {
        String palabra = "";
        String encriptador = "😘";
        int i = 0;
        char[] enc = encriptador.toCharArray();
        for (char letra : s.toCharArray()) {
            palabra += (char) ((letra - enc[i]));
            i++;
            if (i >= enc.length - 1) {
                i = 0;
            }
        }
        return palabra;
    }

}