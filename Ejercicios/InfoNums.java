public class InfoNums {
    static int[] numeros;

    public static void main(String[] args) {
        numeros = new int[ args.length];
        for (int i = 0; i < args.length; i++) {
            numeros[i] = Integer.parseInt(args[i]);

        }
        reproducir_numeros();
        numero_mayor();
        numero_menor();
        numero_media();
    }

    static void reproducir_numeros() {
        System.out.println("Hay " + numeros.length + " numeros.");
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + " | ");

        }
    }

    static void numero_mayor() {
        int mayor  = numeros[0];
        for (int i = 0; i < numeros.length; i++) {
            if(mayor < numeros[i]){
                mayor = numeros[i];
            }else{
                continue;
            }
        }
        System.out.println("\nEl mayor es " + mayor);
    }

    static void numero_menor() {
        int menor = numeros[0];
        for (int i = 0; i < numeros.length; i++) {
            if(menor > numeros[i]){
                menor = numeros[i];
            }else{
                continue;
            }
        }
        System.out.println("\nEl menor es " + menor);
    }

    static void numero_media() {
        float total = 0;
        for (int i = 0; i < numeros.length; i++) {
            total += numeros[i];

        }

        System.out.println("\nTotal de los numeros: " + total + " la media es:" + (total / numeros.length));

    }

}