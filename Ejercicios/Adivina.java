import java.util.Scanner;
//package a importar al principio del archivo de clase
import java.util.Random;

public class Adivina {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        
        int num = 0;
        boolean match = false;
        int incognita = random.nextInt(10) + 1;
        int contador = 0;
        do {
            System.out.printf("Entra un num: ");
            try {
               
                num = keyboard.nextInt();
                //  System.out.println(" Incognita:" + incognita);
                if (num == incognita) {
                    match = true;
                }else{
                    contador++;
                }

            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (match == false);
        System.out.println("Haz necesitado "+ contador + " oportunidades");
    }
}