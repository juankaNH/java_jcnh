import java.util.Scanner;
public class Jugadores{

    String nombre;
    int partidas_ganadas;

    public Jugadores(){
        Scanner entrada = new Scanner(System.in);       //Crea objeto de entrada de datos
        System.out.println("Introduce tu nombre ");
        String entra = entrada.nextLine();              //Pasa la entrada a una cadena String
        this.nombre = entra;
    }

    void saluda(){
        System.out.println("Hola soy " + this.nombre);
    }

    String jugada(Jugadores jugador){
        Scanner entrada = new Scanner(System.in);       //Crea objeto de entrada de datos
        System.out.println("Introduce tu jugada jugador " + jugador.nombre + " : ");
        String entra = entrada.nextLine();              //Pasa la entrada a una cadena String
        Limpiza_de_terminal();
        return entra;
    }
    public static void Limpiza_de_terminal(){       //Proceso para limpiar el terminal
        try{
         new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        }catch(Exception e){
 
        }
     }
}

