public class Tablero{

    String matriz[][] = new String[4][4];
    boolean partida = false;

    public Tablero(){

        this.matriz[0][0] = "[]";
        this.matriz[0][1] = "[A]";
        this.matriz[0][2] = "[B]";
        this.matriz[0][3] = "[C]";
        this.matriz[1][0] = "[1]";
        this.matriz[1][1] = "[-]";
        this.matriz[1][2] = "[-]";
        this.matriz[1][3] = "[-]";
        this.matriz[2][0] = "[2]";
        this.matriz[2][1] = "[-]";
        this.matriz[2][2] = "[-]";
        this.matriz[2][3] = "[-]";
        this.matriz[3][0] = "[3]";
        this.matriz[3][1] = "[-]";
        this.matriz[3][2] = "[-]";
        this.matriz[3][3] = "[-]";
    }

    public void printa_tablero(){
        for (int e = 0; e < this.matriz.length; e++) {
            for (int i = 0; i < this.matriz.length; i++) {
                System.out.print("\t" + matriz[i][e]);
            }
            System.out.print("\n");
        }
    }

    public boolean Comprovacion_de_la_jugada(String result, int jugador){
        String signo = "";

        // Augmenta el contador de la jugada
        // contador_jugada();

        // Printa información

        System.out.println("Jugada: " + result + " del jugador: " + jugador);

        // Elige el signo
        // Jugador 1 = [x]      Jugador 2 = [o]
        if (jugador == 1) {
            signo = "[x]";
        } else {
            signo = "[o]";
        }

        // Dibuja la jugada

        /*
         Busca la casilla de la matriz a partir de la entrada de teclado recibido (result)
         si el primer caracter (result.charAt(0)) es = a una fila entra a mira las columanas (result.charAt(1) == 'x')(x = 1 o 2 o 3)

        COMPRUEBA SI ESTA VACIO [-] 
        si esta vacio lo cambia si no lo pone vacio y PIERDA LA JUGADA!!
        */
        if (result.length() <= 2) {
            if (result.charAt(0) == 'A' | result.charAt(0) == 'a') {
                if (result.charAt(1) == '1') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[1][1] == "[-]") {
                        this.matriz[1][1] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[1][1] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }

                } else if (result.charAt(1) == '2') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[2][1] == "[-]") {
                        this.matriz[2][1] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[2][1] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '3') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[3][1] == "[-]") {
                        this.matriz[3][1] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[3][1] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else {
                    System.out.println("Columna erronea!!");
                }

            } else if (result.charAt(0) == 'B' | result.charAt(0) == 'b') {
                if (result.charAt(1) == '1') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[1][2] == "[-]") {
                        this.matriz[1][2] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[1][2] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '2') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[2][2] == "[-]") {
                        this.matriz[2][2] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[2][2] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '3') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[3][2] == "[-]") {
                        this.matriz[3][2] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[3][2] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else {
                    System.out.println("Columna erronea!!");
                }
            } else if (result.charAt(0) == 'C' | result.charAt(0) == 'c') {
                if (result.charAt(1) == '1') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[1][3] == "[-]") {
                        this.matriz[1][3] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[1][3] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '2') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[2][3] == "[-]") {
                        this.matriz[2][3] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[2][3] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '3') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (this.matriz[3][3] == "[-]") {
                        this.matriz[3][3] = signo;   //Asigna el signo del jugado
                        
                    } else {
                        this.matriz[3][3] = "[-]";   //Pone la posicion en vacio
                        Partida.numero_de_jugada--;
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else {
                    System.out.println("Columna erronea!!");    //Fayo en el numero de columna
                    return false;
                }
            } else {
                System.out.println("Fila erronea!!");       //Fayo en el numero de fila
                return false;
            }
        }
        estado_de_la_partida();
        return true;

    }

    
    public void estado_de_la_partida() { // Comprueba si hay un ganador

        // COMPROVACIONES
        // 111 000 000 100 010 001 100 001
        // 000 111 000 100 010 001 010 010
        // 000 000 111 100 010 001 001 100
        // ----------------------------------

        if (this.matriz[1][1] == "[x]" & this.matriz[2][1] == "[x]" & this.matriz[3][1] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][2] == "[x]" & this.matriz[2][2] == "[x]" & this.matriz[3][2] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][3] == "[x]" & this.matriz[2][3] == "[x]" & this.matriz[3][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][1] == "[x]" & this.matriz[1][2] == "[x]" & this.matriz[1][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[2][1] == "[x]" & this.matriz[2][2] == "[x]" & this.matriz[2][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[3][1] == "[x]" & this.matriz[3][2] == "[x]" & this.matriz[3][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][1] == "[x]" & this.matriz[2][2] == "[x]" & this.matriz[3][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[3][1] == "[x]" & this.matriz[2][2] == "[x]" & this.matriz[1][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][1] == "[o]" & this.matriz[2][1] == "[o]" & this.matriz[3][1] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][2] == "[o]" & this.matriz[2][2] == "[o]" & this.matriz[3][2] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][3] == "[o]" & this.matriz[2][3] == "[o]" & this.matriz[3][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][1] == "[o]" & this.matriz[1][2] == "[o]" & this.matriz[1][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[2][1] == "[o]" & this.matriz[2][2] == "[o]" & this.matriz[2][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[3][1] == "[o]" & this.matriz[3][2] == "[o]" & this.matriz[3][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[1][1] == "[o]" & this.matriz[2][2] == "[o]" & this.matriz[3][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        } else if (this.matriz[3][1] == "[o]" & this.matriz[2][2] == "[o]" & this.matriz[1][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            this.partida = true; 
        }
    }

}