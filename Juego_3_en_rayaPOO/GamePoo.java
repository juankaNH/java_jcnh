/**
 * Juego de 3 en raya con objetos
 * 
 * @author JuankaNH
 * 
 */
public class GamePoo {

    public static void main(String[] args) {
        
        Jugadores p1 = new Jugadores();
        
        Jugadores p2 = new Jugadores();
        
        Partida party = new Partida(p1,p2);
        Limpiza_de_terminal();
        party.start();
        
    }

    public static void Limpiza_de_terminal(){       //Proceso para limpiar el terminal
        try{
         new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        }catch(Exception e){
 
        }
     }
}