
public class Partida {

    int numero_de_partidas = 0;
    Jugadores[] num_jugadores = new Jugadores[2];
    Tablero mesa_de_juego = new Tablero();
    int turno_jugador;
    static int numero_de_jugada = 0;

    public Partida(Jugadores j1, Jugadores j2) {

        this.num_jugadores[0] = j1;
        this.num_jugadores[1] = j2;
    }

    void start() {
        System.out.println("Hola Jugador " + this.num_jugadores[0].nombre + " y " + this.num_jugadores[1].nombre);
        System.out.println("\t --------------------");
        System.out.println("\t --Empieza el Juego--");
        System.out.println("\t --------------------\n");
        this.mesa_de_juego.printa_tablero();
        turno_jugador = eleccion_de_jugador();
        do {
            if (this.mesa_de_juego.Comprovacion_de_la_jugada(
                    this.num_jugadores[(turno_jugador - 1)].jugada(this.num_jugadores[(turno_jugador - 1)]), turno_jugador)) {
                this.mesa_de_juego.printa_tablero();
                this.numero_de_jugada++;
                Comprueba_fin_partida();
            } else {
                this.mesa_de_juego.Comprovacion_de_la_jugada(
                        this.num_jugadores[(turno_jugador - 1)].jugada(this.num_jugadores[(turno_jugador - 1)]), turno_jugador);
                this.mesa_de_juego.printa_tablero();
                Comprueba_fin_partida();
            }
            cambio_De_jugador();
        } while (this.numero_de_jugada < 9);

        System.out.println("FINAL DE LA PARTIDA!!");
    }

    public void Comprueba_fin_partida() {
        if(this.mesa_de_juego.partida){
            this.numero_de_jugada = 10;
        }
    }

    public void cambio_De_jugador() {
        if (this.turno_jugador == 1) {
            this.turno_jugador = 2;
        } else {
            this.turno_jugador = 1;
        }
    }

    private int eleccion_de_jugador() {

        int valorDado = (int) (Math.random() * 2) + 1;
        // System.out.println("Valor random: " + valorDado);
        return valorDado;
    }
}