import java.io.CharArrayReader;
import java.io.IOException;
import java.util.Scanner;
import java.io.*;

public class Game {

    static String matriz[][] = new String[4][4];
    static boolean jugador = true;
    static int numero_juagada = 0;

    public static void main(String[] args) {
        Limpiza_de_terminal();
        inicio_game();
        printa_tablero();
        System.out.println("\nJugador " + get_jugador() + " Introduce posicion: (por ejemplo: A1)");
        do {
            System.out.println("\nJugador " + get_jugador() + " Introduce posicion:");
            Scanner entrada = new Scanner(System.in);       //Crea objeto de entrada de datos
            String entra = entrada.nextLine();              //Pasa la entrada a una cadena String
            Limpiza_de_terminal();
            jugada(entra);
            printa_tablero();
        } while (numero_juagada < 9);
        fin_de_juego();
    }

    public static void fin_de_juego() { // Mensaje de final de partida
        System.out.println("\n\n\tFINAL DE LA PARTIDA!!\n\n");
    }

    public static int get_jugador() { // Devuelbe el jugador actual
        if (jugador) {
            return 1;
        } else {
            return 2;
        }
    }

    public static void cambio_jugador() { // Cambia entre jugadores
        if (jugador) {
            jugador = false;
        } else {
            jugador = true;
        }
    }

    public static void inicio_game() { // Declaracion de variables iniciales
        System.out.println("\t --------------------");
        System.out.println("\t --Empieza el Juego--");
        System.out.println("\t --------------------\n");
        matriz[0][0] = "[]";
        matriz[0][1] = "[A]";
        matriz[0][2] = "[B]";
        matriz[0][3] = "[C]";
        matriz[1][0] = "[1]";
        matriz[1][1] = "[-]";
        matriz[1][2] = "[-]";
        matriz[1][3] = "[-]";
        matriz[2][0] = "[2]";
        matriz[2][1] = "[-]";
        matriz[2][2] = "[-]";
        matriz[2][3] = "[-]";
        matriz[3][0] = "[3]";
        matriz[3][1] = "[-]";
        matriz[3][2] = "[-]";
        matriz[3][3] = "[-]";
    }

    public static void printa_tablero() { // Dibuja el estado del tablero
        
        // TABLERO      static String matriz[][] = new String[4][4];
        // [] [1] [2] [3]
        // [A] [-] [-] [-]
        // [B] [-] [-] [-]
        // [c] [-] [-] [-]
        //----------------------------
        // La creacion del dibujo del tablero se hace en inicio_game()
        //----------------------------
        // BORRADO DE PANTALLA FAYA
        // try{
        // String[] comand = {"cmd.exe","cls"};
        // Runtime.getRuntime().exec(comand,null);
        // }catch(IOException e){
        // e.printStackTrace();
        // }
        // --------------------------------------------------
        for (int e = 0; e < matriz.length; e++) {
            for (int i = 0; i < matriz.length; i++) {
                System.out.print("\t" + matriz[i][e]);
            }
            System.out.print("\n");
        }
    }

    public static void contador_jugada() { // Augmenta el contador de la jugada
        numero_juagada++;
    }

    public static void jugada(String result) { // crea todos los movimientos de la jugada indicada
        String signo = "";

        // Augmenta el contador de la jugada
        contador_jugada();

        // Printa información

        System.out.println("Jugada: " + result + " del jugador: " + get_jugador());

        // Elige el signo
        // Jugador 1 = [x]      Jugador 2 = [o]
        if (get_jugador() == 1) {
            signo = "[x]";
        } else {
            signo = "[o]";
        }

        // Dibuja la jugada

        /*
         Busca la casilla de la matriz a partir de la entrada de teclado recibido (result)
         si el primer caracter (result.charAt(0)) es = a una fila entra a mira las columanas (result.charAt(1) == 'x')(x = 1 o 2 o 3)

        COMPRUEBA SI ESTA VACIO [-] 
        si esta vacio lo cambia si no lo pone vacio y PIERDA LA JUGADA!!
        */
        if (result.length() <= 2) {
            if (result.charAt(0) == 'A' | result.charAt(0) == 'a') {
                if (result.charAt(1) == '1') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[1][1] == "[-]") {
                        matriz[1][1] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[1][1] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }

                } else if (result.charAt(1) == '2') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[2][1] == "[-]") {
                        matriz[2][1] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[2][1] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '3') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[3][1] == "[-]") {
                        matriz[3][1] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[3][1] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else {
                    System.out.println("Columna erronea!!");
                }

            } else if (result.charAt(0) == 'B' | result.charAt(0) == 'b') {
                if (result.charAt(1) == '1') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[1][2] == "[-]") {
                        matriz[1][2] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[1][2] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '2') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[2][2] == "[-]") {
                        matriz[2][2] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[2][2] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '3') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[3][2] == "[-]") {
                        matriz[3][2] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[3][2] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else {
                    System.out.println("Columna erronea!!");
                }
            } else if (result.charAt(0) == 'C' | result.charAt(0) == 'c') {
                if (result.charAt(1) == '1') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[1][3] == "[-]") {
                        matriz[1][3] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[1][3] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '2') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[2][3] == "[-]") {
                        matriz[2][3] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[2][3] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else if (result.charAt(1) == '3') {
                    //COMPROVACION DE SI ESTA VACIO
                    if (matriz[3][3] == "[-]") {
                        matriz[3][3] = signo;   //Asigna el signo del jugado
                    } else {
                        matriz[3][3] = "[-]";   //Pone la posicion en vacio
                        numero_juagada--;       //restamos la jugada como si no ubiese tirado
                        System.out.println(" Pasa tu turno a cambio del sitio....");
                    }
                } else {
                    System.out.println("Columna erronea!!");    //Fayo en el numero de columna
                }
            } else {
                System.out.println("Fila erronea!!");       //Fayo en el numero de fila
            }
        }
        estado_de_la_partida();

        cambio_jugador();

    }

    public static void estado_de_la_partida() { // Comprueba si hay un ganador

        // COMPROVACIONES
        // 111 000 000 100 010 001 100 001
        // 000 111 000 100 010 001 010 010
        // 000 000 111 100 010 001 001 100
        // ----------------------------------

        if (matriz[1][1] == "[x]" & matriz[2][1] == "[x]" & matriz[3][1] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][2] == "[x]" & matriz[2][2] == "[x]" & matriz[3][2] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][3] == "[x]" & matriz[2][3] == "[x]" & matriz[3][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][1] == "[x]" & matriz[1][2] == "[x]" & matriz[1][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[2][1] == "[x]" & matriz[2][2] == "[x]" & matriz[2][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[3][1] == "[x]" & matriz[3][2] == "[x]" & matriz[3][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][1] == "[x]" & matriz[2][2] == "[x]" & matriz[3][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[3][1] == "[x]" & matriz[2][2] == "[x]" & matriz[1][3] == "[x]") {
            System.out.println("\n\n\tGANA EL JUGADOR 1\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][1] == "[o]" & matriz[2][1] == "[o]" & matriz[3][1] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][2] == "[o]" & matriz[2][2] == "[o]" & matriz[3][2] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][3] == "[o]" & matriz[2][3] == "[o]" & matriz[3][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][1] == "[o]" & matriz[1][2] == "[o]" & matriz[1][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[2][1] == "[o]" & matriz[2][2] == "[o]" & matriz[2][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[3][1] == "[o]" & matriz[3][2] == "[o]" & matriz[3][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[1][1] == "[o]" & matriz[2][2] == "[o]" & matriz[3][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        } else if (matriz[3][1] == "[o]" & matriz[2][2] == "[o]" & matriz[1][3] == "[o]") {
            System.out.println("\n\n\tGANA EL JUGADOR 2\n\n"); // Printa en pantalla
            numero_juagada = 10; // Sale de la partida
        }
    }

    public static void Limpiza_de_terminal(){       //Proceso para limpiar el terminal
        try{
         new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        }catch(Exception e){
 
        }
     }
}